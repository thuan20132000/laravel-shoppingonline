$('document').ready(function() {



    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#uploadForm + img').remove();
                $('#uploadForm').after('<img src="' + e.target.result + '" width="220" height="220"/>');
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#file").change(function() {
        filePreview(this);
    });



    // delete One category
    $('.deleteItem').on('click', function(e) {
        let categoryId = $(this).attr("data-id");


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({

            method: 'DELETE',
            url: 'category/' + categoryId,
            success: function(data) {

                $(document).Toasts('create', {
                    title: 'Deleted successful',
                    class: 'toast-delete',
                    delay: 2000,
                    autohide: true
                });
                setTimeout(() => {
                    location.reload();
                }, 1200);

            }

        });
    });

    $('.deleteProduct').on('click', function(e) {
        let productId = $(this).attr("data-id");


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({

            method: 'DELETE',
            url: 'product/' + productId,
            success: function(data) {

                $(document).Toasts('create', {
                    title: 'Deleted successful',
                    class: 'toast-delete',
                    delay: 2000,
                    autohide: true
                });
                setTimeout(() => {
                    location.reload();
                }, 1200);

            }

        });
    });

    $('.deletevendor').on('click', function(e) {
        let vendorId = $(this).attr("data-id");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({

            method: 'DELETE',
            url: 'vendor/' + vendorId,
            success: function(data) {
                if (!data.data) {
                    $(document).Toasts('create', {
                        title: 'Deleted successful',
                        class: 'toast-delete',
                        delay: 2000,
                        autohide: true
                    });
                    setTimeout(() => {
                        location.reload();
                    }, 1200);
                }


            }

        });
    });
    $('.deletepromotion').on('click', function(e) {
        let promotionId = $(this).attr("data-id");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({

            method: 'DELETE',
            url: 'promotion/' + promotionId,
            success: function(data) {
                if (!data.data) {
                    $(document).Toasts('create', {
                        title: 'Deleted successful',
                        class: 'toast-delete',
                        delay: 2000,
                        autohide: true
                    });
                    setTimeout(() => {
                        location.reload();
                    }, 1200);
                }


            }

        });
    });

    $('.deleteorder').on('click', function(e) {
        let orderId = $(this).attr("data-id");
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({

            method: 'DELETE',
            url: 'order/' + orderId,
            success: function(data) {
                if (!data.data) {
                    toastr.success('Delete order Successfull', { timeOut: 3000 })
                    setTimeout(() => {
                        location.reload();
                    }, 1200);
                }


            }

        });
    });



    //checkbox Category
    $('#checkCategory-all').on('click', function() {
        $('input[name="category"]').prop('checked', this.checked);
    })

    $('#deleteCategory-checked').on('click', function() {
        var categoryArray = [];
        $.each($("input[name='category']:checked"), function() {
            categoryArray.push($(this).val());
        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({

            method: 'DELETE',
            url: 'category/',
            data: {
                'ids': categoryArray
            },
            success: function(data) {

                if (!data.data) {
                    $(document).Toasts('create', {
                        title: 'Deleted successful',
                        class: 'toast-delete',
                        delay: 2000,
                        autohide: true
                    });

                    setTimeout(() => {
                        location.reload();
                    }, 1200);
                }

            }

        });

    });


    //checkbox Product
    $('#checkProduct-all').on('click', function() {
        $('input[name="product"]').prop('checked', this.checked);
    });

    $('#deleteProduct-checked').on('click', function() {
        var productArray = [];
        $.each($("input[name='product']:checked"), function() {
            productArray.push($(this).val());
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            method: 'DELETE',
            url: 'product/',
            data: {
                'ids': productArray
            },
            success: function(data) {
                if (!data.data) {
                    $(document).Toasts('create', {
                        title: 'Deleted successful',
                        class: 'toast-delete',
                        delay: 2000,
                        autohide: true
                    });

                    setTimeout(() => {
                        location.reload();
                    }, 1200);
                }


            }

        });

    });


    //Vendor
    $('#checkvendor-all').on('click', function() {
        $('input[name="vendor"]').prop('checked', this.checked);
    });

    $('#deletevendor-checked').on('click', function() {
        var productArray = [];
        $.each($("input[name='vendor']:checked"), function() {
            productArray.push($(this).val());
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            method: 'DELETE',
            url: 'vendor/',
            data: {
                'ids': productArray
            },
            success: function(data) {
                if (!data.data) {
                    $(document).Toasts('create', {
                        title: 'Deleted successful',
                        class: 'toast-delete',
                        delay: 2000,
                        autohide: true
                    });

                    setTimeout(() => {
                        location.reload();
                    }, 1200);
                }


            }

        });

    });

    //Promotion
    $('#checkpromotion-all').on('click', function() {
        $('input[name="promotion"]').prop('checked', this.checked);
    });

    $('#deletepromotion-checked').on('click', function() {
        var promotionArray = [];
        $.each($("input[name='promotion']:checked"), function() {
            promotionArray.push($(this).val());
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            method: 'DELETE',
            url: 'promotion/',
            data: {
                'ids': promotionArray
            },
            success: function(data) {
                if (!data.data) {
                    $(document).Toasts('create', {
                        title: 'Deleted successful',
                        class: 'toast-delete',
                        delay: 2000,
                        autohide: true
                    });


                    setTimeout(() => {
                        location.reload();
                    }, 1200);
                }


            }

        });

    });

    // checkall order
    $('#checkorder-all').on('click', function() {
        $('input[name="order"]').prop('checked', this.checked);
    });

    $('#deleteorder-checked').on('click', function() {
        var orderArray = [];
        $.each($("input[name='order']:checked"), function() {
            orderArray.push($(this).val());
        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            method: 'DELETE',
            url: 'order/',
            data: {
                'ids': orderArray
            },
            success: function(data) {
                if (!data.data) {
                    toastr.success('Delete Successfull', { timeOut: 3000 })
                    setTimeout(() => {
                        location.reload();
                    }, 1200);
                }
            }

        });

    });

    // cookie for active menu
    var cookieValue = document.cookie.replace(/(?:(?:^|.*;\s*)menu-active\s*\=\s*([^;]*).*$)|^.*$/, "$1");
    $('.link-menu').on('click', function() {
        if ($(this).parent().is('.menu-open')) {
            document.cookie = 'menu-active=' + 0;

        } else {
            $(this).parent().addClass('menu-active');
            $("li.nav-item.has-treeview").each(function(index, element) {
                if ($(this).is('.menu-active')) {
                    document.cookie = 'menu-active=' + index;

                }
            });
        }


    });


    $("li.nav-item.has-treeview").each(function(index, element) {
        var menuActive = parseInt(cookieValue, 10);
        if (index == menuActive) {
            $(this).addClass('menu-open');
        }
    });

    if ($('#denied').length) {
        alert("Can't acssess to this function!!");
    }


});