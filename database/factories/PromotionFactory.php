<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Promotion;
use Faker\Generator as Faker;
use Faker\Provider\DateTime;
use Faker\Provider\Payment;


$factory->define(Promotion::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->word,
        'description' => $faker->paragraph,
        'start_date' => $faker->date('Y-m-d'),
        'end_date' => $faker->date('Y-m-d'),
        'promotionCode' => $faker->swiftBicNumber,
        'promotionPrice' => $faker->numberBetween(2, 10),

    ];
});
