<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Order;
use App\User;

use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        //
        'subtotal' => $faker->numberBetween(20, 120),
        'shipping' => $faker->numberBetween(5, 10),
        'discount' => $faker->numberBetween(5, 10),
        'payment_method' => $faker->randomElement(array('Cash', 'Paypal', 'Momo', 'ViettelPay')),
        'user_id' => function () {
            return  User::all()->random();
        }
    ];
});
