<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Vendor;
use Faker\Generator as Faker;
use Faker\Provider\es_ES\PhoneNumber;
use Faker\Provider\fr_FR\Address;



$factory->define(Vendor::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'phone' => PhoneNumber::mobileNumber(),
        'address' => Address::region(),

    ];
});
