<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model\Category;
use App\Model\Product;
use App\Model\Promotion;
use App\Model\Vendor;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->word,
        'slug' => $faker->slug,
        'description' => $faker->paragraph,
        'stock' => $faker->numberBetween(12, 120),
        // 'image' => $faker->imageUrl,
        'price' => $faker->numberBetween(100, 300),
        'status' => 1,
        'category_id' => function () {
            return Category::all()->random();
        },
        'vendor_id' => function () {
            return Vendor::all()->random();
        },
        'promotion_id' => function () {
            return Promotion::all()->random();
        }

    ];
});
