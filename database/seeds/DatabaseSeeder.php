<?php

use App\Model\Category;
use App\Model\Product;
use App\Model\Promotion;
use App\Model\Vendor;
use App\Model\Order;
use App\Model\Review;
use App\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserSeeder::class);
        factory(Promotion::class, 10)->create();
        factory(Vendor::class, 10)->create();
        factory(Category::class, 10)->create();
        factory(Product::class, 50)->create();
        // factory(Order::class, 50)->create();
        // factory(User::class, 10)->create();
        // factory(Review::class, 50)->create();
    }
}
