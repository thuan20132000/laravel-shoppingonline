<?php

use Illuminate\Database\Seeder;
use App\Model\Role;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // User::truncate();
        DB::table('role_user')->truncate();

        $adminRole = Role::where('name', 'admin')->first();
        $authorRole = Role::where('name', 'author')->first();
        $userRole = Role::where('name', 'user')->first();

        $admin = User::create([
            'name' => 'Admin User',
            'email' => 'Admin@gmail.com',
            'password' => Hash::make('adminadmin'),
        ]);
        $author = User::create([
            'name' => 'Author User',
            'email' => 'Author@gmail.com',
            'password' => Hash::make('authorauthor'),
        ]);
        $user = User::create([
            'name' => ' User',
            'email' => 'User@gmail.com',
            'password' => Hash::make('useruser'),
        ]);

        $admin->roles()->attach($adminRole);
        $author->roles()->attach($authorRole);
        $user->roles()->attach($userRole);
    }
}
