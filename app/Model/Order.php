<?php

namespace App\Model;

use App\User;
use App\Model\OrderDetail;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function orderDetail()
    {
        return $this->hasMany(OrderDetail::class);
    }

    public function address(){
        return $this->hasOne(OrderAddress::class);
    }
}
