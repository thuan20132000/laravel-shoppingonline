<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    //
    protected $fillable = [
        'name', 'address', 'phone', 'image'
    ];


    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
