<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Review\ReviewCollection;
use App\Model\Review;
use App\Model\Product;
use Illuminate\Http\Request;

class apiReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Product $product)
    {
        //
        return ReviewCollection::collection($product->review);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Product $product)
    {
        //
        $review = new Review();
        $review->product_id = $product->id;
        $review->user_id = $request->user_id;
        $review->review = $request->review;
        $review->star = $request->star;
        $review->save();
        return response([
            'data' => new ReviewCollection($review)
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product, Review $review)
    {
        //
        $review->product_id = $product->id;
        $review->user_id = $request->user_id;
        $review->review = $request->review;
        $review->star = $request->star;
        $review->save();
        return response([
            'data' => new ReviewCollection($review)
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
