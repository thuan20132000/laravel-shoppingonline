<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Order;
use App\Model\OrderDetail;
use App\Model\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;


class apiOrderController extends Controller
{
    //

    public function saveOrder(Request $request,User $user){


        $getProducts = array();
        $totalPrice = null;
        $payment = null;
        $discount = null;
        $errors = null;
        $products = $request->cartItems;
        foreach ($products as $key => $value) {
            array_push($getProducts,$value);
        }
        $totalPrice = $request->totalAmount;
        if($request->discount){
            $discount = $request->discount;
            $totalPrice = $totalPrice - $discount;
        }
        $currentDateTime = Carbon::now('Asia/Ho_Chi_Minh');
        $orderId = DB::table('orders')->insertGetId(
            array('subtotal' => $totalPrice, 'user_id' => $user->id,'discount'=>$discount,'payment_method'=>$payment,'created_at'=>$currentDateTime,'updated_at'=>$currentDateTime)
        );

        foreach ($getProducts as $key => $value) {
            # code...
            $orderDetail = new OrderDetail();
            $orderDetail->quantity = $value['productQuantity'];
            $orderDetail->product_id = $value['productId'];
            try {
                //code...
                $product = Product::find($value['productId']);
                $product->stock = $product->stock - $value['productQuantity'];
                $product->save();
            } catch (\Throwable $th) {
                throw $errors =  $th;
            }
            $orderDetail->order_id = $orderId;
            $orderDetail->save();

        }

        $totalPrice = $request->totalAmount;
        $data['date'] = $request->date;

        if($errors){
            return response(['error'=>$errors],404);
        }

        return response(['success',$orderId],200);
    }


}
