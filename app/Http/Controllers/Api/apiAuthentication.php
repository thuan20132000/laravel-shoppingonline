<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class apiAuthentication extends Controller
{
    //
    public $successStatus = 200;


    // Add data after login: Name , Id,
    public function login(Request $request){
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $success['id'] = $user->id;
            $succes['name'] = $user->name;
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            return response()->json(['success' => $success], $this->successStatus);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }

    }

    // Add data after register: Name , Id,
    public function register(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $error_email_exits= null;
        try {
            //code...
            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
            $user = User::create($input);
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['name'] =  $user->name;
            $success['id'] = $user->id;
        } catch (\Throwable $th) {
            throw $error_email_exits =  $th;
        }
        // if(!$error_email_exits){
        //     return response()->json(['error'=>$error_email_exits], 401);
        // }
        return response()->json(['success'=>$success], $this->successStatus);

    }
}
