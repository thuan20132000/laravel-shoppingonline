<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Resources\Product\ProductCollection;
use App\Http\Resources\Product\ProductResource;
use App\Model\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Str;


class apiProductController extends Controller
{



    public function __construct()
    {
        $this->middleware('auth:api')->except('index', 'show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if ($request->input('category')) {
            return ProductCollection::collection(Product::where('category_id', $request->input('category'))->get());
        }
        return  ProductCollection::collection(Product::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request)
    {
        //
        $product  = new Product();
        $product->name = $request->name;
        $product->slug = Str::slug($request->name);
        $product->stock = $request->stock;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->vendor_id = $request->vendor_id;
        $product->category_id = $request->category_id;
        $product->save();
        return response()->json([
            'data' => new ProductResource($product),
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $product = Product::find($id);
        return new ProductResource($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
        $product->description = $request->description;
        $product->price = $request->price;
        $product->stock = $request->stock;
        $product->promotion_id = $request->promotion;
        $product->save();

        return response()->json([
            'data' => new ProductResource($product)
        ], 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
        $product->delete();
        return response()->json([
            'data' => null,
        ], 204);
    }
}
