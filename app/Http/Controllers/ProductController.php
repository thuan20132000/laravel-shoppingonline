<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\Product;
use App\Model\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;

use Aws\Credentials\Credentials;
use Aws\S3\S3Client;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $products = Product::orderBy('id', 'desc')->paginate(20);
        return view("admin.product.index", ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::all();
        $vendors = Vendor::all();
        return view('admin.product.create', ['categories' => $categories, 'vendors' => $vendors]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validated = $request->validate([
            'name' => 'required',
            'category' => 'required',
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
            'vendor' => 'required'
        ], [
            'name.required' => 'Product name was empty',
            'category.required' => 'Category was empty',
            'price.required' => 'Price was empty',
            'price.numeric' => 'Price have to be number',
            'stock.required' => 'Quantity was empty',
            'stock.numeric' => 'Quantity have to be number',
            'vendor.required' => 'Vendor was empty',
        ]);



        $imageUrl = 'default.jpg';
        if ($request->hasFile('image')) {
            $file       = $request->file('image');
            $filename    = Str::slug($request->name) . '-' . time() . '.' . $file->getClientOriginalExtension();

            $img = Image::make($file);

            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->text('CDIO4 e-commerce', 10, 10, function ($text) {
                $text->color('#db1c1c');
            });

            //detach method is the key! Hours to find it... :/
            $resource = $img->stream()->detach();
            Storage::disk('s3')->put('s3-images/' . $filename, $resource);
            $imageUrl = "https://laravel-shopping-app.s3.ap-southeast-1.amazonaws.com/s3-images/" . $filename;
        }

        $product = new Product();
        $product->name = $request->name;
        $product->description = $request->description;
        $product->slug = Str::slug($request->name);
        $product->price = $request->price;
        $product->stock = $request->stock;
        $product->status = $request->status == 'on' ? 1 : 0;
        $product->image = $imageUrl;
        $product->category_id = $request->category;
        $product->vendor_id = $request->vendor;

        $product->save();

        return redirect()->back()->with('success', 'Added Product Successfull');
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
        $vendors = Vendor::all();
        $categories = Category::all();
        return view('admin.product.edit', ['product' => $product, 'categories' => $categories, 'vendors' => $vendors]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
        $validated = $request->validate([
            'name' => 'required',
            'category' => 'required',
            'price' => 'required|numeric',
            'stock' => 'required|numeric',
        ], [
            'name.required' => 'Product name was empty',
            'category.required' => 'Category was empty',
            'price.required' => 'Price was empty',
            'price.numeric' => 'Price have to be number',
            'stock.required' => 'Quantity was empty',
            'stock.numeric' => 'Quantity have to be number'
        ]);


        $imageUrl = 'default.jpg';
        if ($request->hasFile('image')) {
            $file       = $request->file('image');
            $filename    = Str::slug($request->name) . '-' . time() . '.' . $file->getClientOriginalExtension();

            $img = Image::make($file);

            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->text('CDIO4 e-commerce', 10, 10, function ($text) {
                $text->color('#db1c1c');
            });

            //detach method is the key! Hours to find it... :/
            $resource = $img->stream()->detach();
            Storage::disk('s3')->put('s3-images/' . $filename, $resource);
            $imageUrl = "https://laravel-shopping-app.s3.ap-southeast-1.amazonaws.com/s3-images/" . $filename;
        }

        $product->name = $request->name;
        $product->description = $request->description;
        $product->slug = Str::slug($request->name);
        $product->price = $request->price;
        $product->stock = $request->stock;
        $product->status = $request->status == 'on' ? 1 : 0;
        if ($imageUrl) {
            $product->image = $imageUrl;
        }
        $product->category_id = $request->category;
        $product->vendor_id = $request->vendor;
        $product->save();

        return redirect()->back()->with('success', 'Updated Successfull');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        //
        $errors = null;
        try {
            Product::find($product->id)->delete();
        } catch (\Throwable $th) {
            $errors = $th;
        }

        return response()->json(['state' => $errors]);
    }

    /**
     * Remove the ALl resource from checkbox.
     *
     * @param  \App\Model\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function deleteAll(Request $request)
    {
        //
        $ids = $request->ids;

        $errors = null;
        try {
            Product::whereIn('id', $ids)->delete();
        } catch (\Throwable $th) {
            $errors = $th;
        }

        return response()->json(['data' => $errors]);
    }
}
