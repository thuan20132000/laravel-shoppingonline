<?php

namespace App\Http\Controllers;

use App\Model\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Providers\RouteServiceProvider;


class Authen extends Controller
{
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    //
    public function showLoginForm(){
        return view('admin.login');
    }

    public function login(){

    }

    public function showRegisterForm(){
        return view('admin.authen.register');
    }

    public function register(Request $request){
        // dd($request->all());
        $validated = $request->validate([
            'name'=>'required|max:255|string',
            'email'=>'required|string|email|max:255',
            'password'=>'required|string|min:8',
        ]);

        // User::create([
        //     'name'=>$request->username,
        //     'email'=>$request->email,
        //     'password'=>Hash::make($request->password),
        // ]);
        $user = new User;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->passowrd);
        $user->remember_token = $request->_token;
        $user->save();

        // $role = Role::select('id')->where('name','user')->first();
        // $user->roles()->attach($role);

        return $user;
    }
}
