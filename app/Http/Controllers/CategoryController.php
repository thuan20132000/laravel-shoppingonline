<?php

namespace App\Http\Controllers;

use App\Http\Requests\RequestCategory;
use App\Model\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = Category::paginate(20);
        return view('admin.category.index', ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'category was empty'
        ]);


        $imageUrl = 'default.jpg';
        if ($request->hasFile('image')) {
            $file       = $request->file('image');
            $filename    = Str::slug($request->name) . '-' . time() . '.' . $file->getClientOriginalExtension();

            $img = Image::make($file);

            $img->resize(300, null, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->text('CDIO4 e-commerce', 10, 10, function ($text) {
                $text->color('#db1c1c');
            });

            //detach method is the key! Hours to find it... :/
            $resource = $img->stream()->detach();
            Storage::disk('s3')->put('s3-images/' . $filename, $resource);
            $imageUrl = "https://laravel-shopping-app.s3-ap-southeast-1.amazonaws.com/s3-images/" . $filename;
        }

        $category = new Category();
        $category->name = $request->name;
        $category->description = $request->description;
        $category->slug = Str::slug($request->name);
        $category->status = $request->status == "on" ? 1 : 0;
        $category->image = $imageUrl;

        $category->save();

        return redirect()->back()->with("success", "Added Category Successfull");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
        return view('admin.category.edit', ['category' => $category]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        //

        $validatedData = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'category was empty'
        ]);

        $category = Category::find($category->id);


        $imageUrl = 'default.jpg';
        if ($request->hasFile('image')) {
            $file       = $request->file('image');
            $filename    = Str::slug($request->name) . '-' . time() . '.' . $file->getClientOriginalExtension();

            $img = Image::make($file);

            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            });

            $img->text('CDIO4 e-commerce', 10, 10, function ($text) {
                $text->color('#db1c1c');
            });

            //detach method is the key! Hours to find it... :/
            $resource = $img->stream()->detach();
            Storage::disk('s3')->put('s3-images/' . $filename, $resource);
            $imageUrl = "https://laravel-shopping-app.s3-ap-southeast-1.amazonaws.com/s3-images/" . $filename;
        }

        $category->name = $request->name;
        $category->description = $request->description;
        $category->slug = Str::slug($request->name);
        $category->status = $request->status == "on" ? 1 : 0;
        if ($imageUrl) {
            $category->image = $imageUrl;
        }
        $category->save();
        return redirect()->back()->with("success", "Updated success full");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        //
        Category::find($category->id)->delete();

        return response()->json(['state' => ''], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function deleteAll(Request $request)
    {
        $ids  = $request->ids;

        $errors = null;
        try {
            Category::whereIn('id', $ids)->delete();
        } catch (\Throwable $th) {
            $errors = $th;
        }

        return response()->json(['data' => $errors]);
    }
}
