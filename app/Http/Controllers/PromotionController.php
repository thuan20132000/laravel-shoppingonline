<?php

namespace App\Http\Controllers;

use App\Model\Promotion;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $promotions = Promotion::orderBy('id', 'desc')->paginate(20);
        return view('admin.promotion.index', ['promotions' => $promotions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.promotion.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validated = $request->validate([
            'name' => 'required',
            'promotionCode' => 'required',
            'promotionPrice' => 'required|numeric',
            'start_date' => 'required',
            'end_date' => 'required'
        ], [
            'name.required' => 'Promotion name was empty',
            'promotionCode.required' => 'Promotion code was empty',
            'promotionPrice.required' => 'Promotion Price was empty',
            'start_date.required' => 'Start Date was empty',
            'end_date.required' => 'End Date was empty',
            'promotionPrice.numeric' => 'Promotion Price have to be number'
        ]);

        $filename = 'default.jpg';
        if ($request->hasFile('image')) {
            $image       = $request->file('image');
            $filename    = Str::slug($request->name) . '-' . time() . '.' . $image->getClientOriginalExtension();

            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(200, 200);
            $image_resize->save(public_path('upload/' . $filename));
        }

        $promotion = new Promotion();
        $promotion->name = $request->name;
        $promotion->description = $request->description;
        $promotion->promotionCode = $request->promotionCode;
        $promotion->promotionPrice = $request->promotionPrice;
        $promotion->start_date = $request->start_date;
        $promotion->end_date = $request->end_date;
        $promotion->image = $filename;

        $promotion->save();
        return redirect()->back()->with('success', 'Added Promotion successfull');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function show(Promotion $promotion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function edit(Promotion $promotion)
    {
        //
        return view('admin.promotion.edit', ['promotion' => $promotion]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Promotion $promotion)
    {
        //
        $validated = $request->validate([
            'name' => 'required',
            'promotionCode' => 'required',
            'promotionPrice' => 'required|numeric',
            'start_date' => 'required',
            'end_date' => 'required'
        ], [
            'name.required' => 'Promotion name was empty',
            'promotionCode.required' => 'Promotion code was empty',
            'promotionPrice.required' => 'Promotion Price was empty',
            'start_date.required' => 'Start Date was empty',
            'end_date.required' => 'End Date was empty',
            'promotionPrice.numeric' => 'Promotion Price have to be number'
        ]);

        $filename = null;
        if ($request->hasFile('image')) {
            $image       = $request->file('image');
            $filename    = Str::slug($request->name) . '-' . time() . '.' . $image->getClientOriginalExtension();

            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(200, 200);
            $image_resize->save(public_path('upload/' . $filename));
        }

        $promotion->name = $request->name;
        $promotion->description = $request->description;
        $promotion->promotionCode = $request->promotionCode;
        $promotion->promotionPrice = $request->promotionPrice;
        $promotion->start_date = $request->start_date;
        $promotion->end_date = $request->end_date;
        if ($filename) {
            $promotion->image = $filename;
        }
        $promotion->save();
        return redirect()->back()->with('success', 'Updated successfull');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Promotion $promotion)
    {
        //
        $errors = null;
        try {
            Promotion::find($promotion->id)->delete();
        } catch (\Throwable $th) {
            $errors = $th;
        }

        return response()->json(['state' => $errors]);
    }

    /**
     * Remove the ALl resource from checkbox.
     *
     * @param  \App\Model\Promotion
     * @return \Illuminate\Http\Response
     */
    public function deleteAll(Request $request)
    {
        //
        $ids = $request->ids;

        $errors = null;
        try {
            Promotion::whereIn('id', $ids)->delete();
        } catch (\Throwable $th) {
            $errors = $th;
        }

        return response()->json(['data' => $errors]);
    }

    // Search Promotion Code ajax
    public function searchPromotionCode(Request $request){
        $data = "";
        if ($request->ajax()) {
            if ($request->get('query')) {
                $query = $request->get('query');
                $data = Promotion::where('promotionCode',$query)->get();
            }
        }

        return response()->json(['data' => $data]);
    }
}
