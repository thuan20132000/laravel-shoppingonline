<?php

namespace App\Http\Controllers;

use App\Model\Order;
use App\Model\OrderAddress;
use App\Model\OrderDetail;
use App\Model\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use SebastianBergmann\Environment\Console;
use Illuminate\Support\Carbon;


class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $orders = Order::orderBy('id', 'desc')->paginate(20);

        return view('admin.orders.index', ['orders' => $orders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        return view('admin.orders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //




        $products = null;
        $customer = null;
        $address = null;
        $errors = null;
        $user_id =null;
        $discount = null;
        $payment = null;
        $getProducts = array();
        if ($request->ajax()) {
            if ($request->get('products')) {
                $data = $request->products;
                foreach ($data as $key => $value) {
                    array_push($getProducts, $value);
                }
            }

            try {

                $totalPrice = $request->totalPrice;
                $user_id = $request->customer['id']?$request->customer['id']:null;

                if(!$user_id){
                    $user_name = $request->customer['name'];
                    $user_email = $request->customer['email'];
                    $user_phone = $request->customer['phone'];
                    $user_pass = Hash::make("12345678");
                    $user_id = DB::table('users')->insertGetId(
                        array('name'=>$user_name,'email'=>$user_email,'password'=>$user_pass,'phone'=>$user_phone)
                    );
                }
                $product = null;
                $currentDateTime = Carbon::now('Asia/Ho_Chi_Minh');

                if($request->discount){
                    $discount = $request->discount;
                    $totalPrice = $totalPrice - $discount;
                }
                if($request->payment){
                    $payment = $request->payment;
                }
                $orderId = DB::table('orders')->insertGetId(
                    array('subtotal' => $totalPrice, 'user_id' => $user_id,'discount'=>$discount,'payment_method'=>$payment,'created_at'=>$currentDateTime,'updated_at'=>$currentDateTime)
                );
                $address = $request->address;
                $orderAddress = new OrderAddress();
                $orderAddress->order_id = $orderId;
                $orderAddress->user_id = $user_id;
                $orderAddress->province = $address['province'];
                $orderAddress->district = $address['district'];
                $orderAddress->subDistrict = $address['subDistrict'];
                $orderAddress->street = $address['street'];
                $orderAddress->address = $address['province'].'-'.$address['district'].'-'.$address['subDistrict'].'-'.$address['street'];
                $orderAddress->save();
                foreach ($getProducts as $key => $value) {
                    $orderDetail = new OrderDetail();
                    $orderDetail->quantity = $value['quantity'];
                    $orderDetail->product_id = $value['id'];
                    try {
                        $product =  Product::find($value['id']);
                        $product->stock = $product->stock - $value['quantity'];
                        $product->save();
                    } catch (\Throwable $th) {
                        throw $errors =  $th;
                    }

                    $orderDetail->order_id = $orderId;
                    $orderDetail->save();
                }

            } catch (\Throwable $th) {
                throw $errors =  $th;
            }
        }



        return response()->json(['data' => $errors]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
        return view('admin.orders.show', ['order' => $order]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
        $errors = null;
        try {
            Order::find($order->id)->delete();
        } catch (\Throwable $th) {
            $errors = $th;
        }

        return response()->json(['state' => $errors]);
    }
    /**
     * Remove the specified resource from storage.
     *
     */
    public function deleteAll(Request $request)
    {
        //
        $ids = $request->ids;

        $errors = null;
        try {
            Order::whereIn('id', $ids)->delete();
        } catch (\Throwable $th) {
            $errors = $th;
        }

        return response()->json(['data' => $errors]);
    }

    /**
     * Search product
     *

     */
    public function searchProduct(Request $request)
    {
        //
        $data = "";
        if ($request->ajax()) {
            if ($request->get('query')) {
                $query = $request->get('query');
                $data = Product::where('name', 'like', '%' . $query . '%')
                    ->orWhere('id', 'like', '%' . $query . '%')->get();
            }
        }

        return response()->json(['data' => $data]);
    }


    /**
     * Search Customer
     *

     */
    public function searchCustomer(Request $request)
    {
        //
        $data = "";
        if ($request->ajax()) {
            if ($request->get('query')) {
                $query = $request->get('query');
                $data = User::where('name', 'like', '%' . $query . '%')
                    ->orWhere('id', 'like', '%' . $query . '%')
                    ->orWhere('email', 'like', '%' . $query . '%')->get();
            }
        }

        return response()->json(['data' => $data]);
    }

    /**
     * Fetch Customer Infor
     *

     */
    public function fetchCustomerInfo(Request $request)
    {
        //
        $customer = "";
        $orderAddress = null;
        if ($request->ajax()) {
            if ($request->get('id')) {
                $customer = User::where('id', $request->id)->get();
                $orderAddress = User::find($request->id)->order_address()->first();
            }
        }

        return response()->json(['data' => $customer,'orderAddress'=>$orderAddress]);
    }
}
