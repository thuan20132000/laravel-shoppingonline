<?php

namespace App\Http\Controllers;

use App\Model\Vendor;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Intervention\Image\ImageManagerStatic as Image;

class VendorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $vendors = Vendor::orderBy('id', 'desc')->paginate(20);
        return view('admin.vendor.index', ['vendors' => $vendors]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.vendor.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validated = $request->validate([
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required|numeric'
        ], [
            'name.required' => 'Vendor was empty',
            'address.required' => 'Vendor Address was empty',
            'phone.required' => 'Vendor phone was empty',
            'phone.numeric' => 'Vendor phone have to be number'
        ]);

        $filename = null;
        if ($request->hasFile('image')) {
            $image       = $request->file('image');
            $filename    = Str::slug($request->name) . '-' . time() . '.' . $image->getClientOriginalExtension();

            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(200, 200);
            $image_resize->save(public_path('upload/' . $filename));
        }

        $vendor = new Vendor();
        $vendor->name = $request->name;
        $vendor->address = $request->address;
        $vendor->phone = $request->phone;
        $vendor->image = $filename;
        $vendor->save();

        return redirect()->back()->with('success', 'Added Vendor Successfull');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function show(Vendor $vendor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function edit(Vendor $vendor)
    {
        //
        return view('admin.vendor.edit', ['vendor' => $vendor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vendor $vendor)
    {
        //
        $validated = $request->validate([
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required|numeric'
        ], [
            'name.required' => 'Vendor was empty',
            'address.required' => 'Vendor Address was empty',
            'phone.required' => 'Vendor phone was empty',
            'phone.numeric' => 'Vendor phone have to be number'
        ]);

        $filename = null;
        if ($request->hasFile('image')) {
            $image       = $request->file('image');
            $filename    = Str::slug($request->name) . '-' . time() . '.' . $image->getClientOriginalExtension();

            $image_resize = Image::make($image->getRealPath());
            $image_resize->resize(200, 200);
            $image_resize->save(public_path('upload/' . $filename));
        }

        $vendor->name = $request->name;
        $vendor->address = $request->address;
        $vendor->phone = $request->phone;
        if ($filename) {
            $vendor->image = $filename;
        }
        $vendor->save();

        return redirect()->back()->with("success", "Updared Vendor Successsful");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vendor $vendor)
    {
        //
        $errors = null;
        try {
            Vendor::find($vendor->id)->delete();
        } catch (\Throwable $th) {
            $errors = $th;
        }

        return response()->json(['data' => $errors]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Vendor  $vendor
     * @return \Illuminate\Http\Response
     */
    public function deleteAll(Request $request)
    {
        //
        $ids = $request->ids;

        $errors = null;
        try {
            Vendor::whereIn('id', $ids)->delete();
        } catch (\Throwable $th) {
            $errors = $th;
        }

        return response()->json(['data' => $errors]);
    }
}
