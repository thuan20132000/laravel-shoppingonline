<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'image' => $this->image,
            'totalPrice' => $this->price,
            'rating' => $this->review->count() > 0 ? round($this->review->sum('star') / $this->review->count(), 2) : 'No Rating Yet',
            'description' => $this->description,
            'href' => [
                'link' => route('products.show', $this->id),
            ]
        ];
    }
}
