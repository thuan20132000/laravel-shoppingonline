<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::apiResource('/products', 'Api\apiProductController');
Route::group(['prefix' => 'products'], function () {
    Route::apiResource('/{product}/reviews', 'Api\apiReviewController');
});

Route::apiResource('/categories', 'Api\apiCategoryController');
Route::apiResource('/promotions','Api\apiPromotionController');

Route::post('login', 'Api\apiAuthenticationController@login');
Route::post('register','Api\apiAuthenticationController@register');

Route::post('order/{user}','Api\apiOrderController@saveOrder');
