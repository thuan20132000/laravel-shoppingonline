<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('/', function () {
    return view('welcome');
});


Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {

    Route::get('/home', function () {
        return view('admin.index');
    })->name('admin.home');

    Route::resource('/category', 'CategoryController');
    Route::delete('/category', 'CategoryController@deleteAll')->name('category.deleteAll');

    Route::resource('/product', 'ProductController');
    Route::delete('/product', 'ProductController@deleteAll')->name('poduct.deleteAll');

    Route::resource('/vendor', 'VendorController');
    Route::delete('/vendor', 'VendorController@deleteAll')->name('vendor.deleteAll');

    Route::resource('/promotion', 'PromotionController');
    Route::delete('/promotion', 'PromotionController@deleteAll')->name('promotion.deleteAll');
    Route::get('searchPromotionCode','PromotionController@searchPromotionCode')->name("promotion.search");

    Route::resource('/order', 'OrderController');
    Route::delete('/order', 'OrderController@deleteAll')->name('order.deleteAll');
    Route::get('searchProducts', 'OrderController@searchProduct')->name('order.searchProduct');
    Route::get('searchCustomers', 'OrderController@searchCustomer')->name('order.searchCustomer');
    Route::get('fetchCustomerInfo', 'OrderController@fetchCustomerInfo')->name('order.fetchCustomerInfo');
});

Auth::routes(['verify' => true]);

// Route::get('/authentication','Authen@showRegisterForm')->name('register');
// Route::post('/authentication','Authen@register')->name('user.register');

Route::namespace('Admin')->prefix('admin')->name('admin.')->middleware('can:manage-users')->group(function () {
    Route::resource('/users', 'UsersController', ['except' => ['show', 'create', 'store']]);
});


Route::get('/home', 'HomeController@index')->name('home')->middleware('verified');
