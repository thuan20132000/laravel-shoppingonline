


@extends('admin.layouts.master')

@section('content')
<div class="content-wrapper">



    <section class="p-3">
            <div class="d-flex justify-content-end" style="width:100%">
                <div style="width:120px;margin-right:20px">
                    <select name="" id=""  class="btn btn-success"  style="width:100%;height:100%">
                        <option value="1">All data</option>
                        <option value="1">1</option>
                        <option value="1">1</option>

                    </select>
                </div>
                <div class="d-flex">
                        <input type="text" value="" placeholder="Search Email or Id" style="width:200px">
                        <button>Search</button>
                </div>
            </div>
    </section>
    <section>
        <div class="d-flex justify-content-between">
            <div style="margin-left:10px;display:flex;flex-direction: row;align-items: center">
                <div><input type="checkbox" name="checkAll" id="checkProduct-all"></div>
                <div style="margin-left:20px"><Button class="btn btn-danger" id="deleteProduct-checked">Delete</Button></div>
                <div style="margin-left:20px"><button class="btn btn-success">Refresh</button></div>
                <div style="margin-left:20px"><button>ID Desc</button></div>
                <div style="margin-left:20px"><button>Sort</button></div>
            </div>
            <div style="margin-right:20px">
                <a href="{{route('product.create')}}" class="btn btn-success">Add new</a>
            </div>
        </div>
    </section>

    <!-- /.box-header -->
    <section id="pjax-container" class="table-list">
    <div class="box-body table-responsive no-padding" >
        <table class="table table-hover">
        <thead>
           <tr>
               <th></th>
               <th>STT</th>
               <th>Name</th>
               <th>Image</th>
               <th>Stock</th>
               <th>Description</th>
               <th>Price</th>
               <th>Status</th>
               <th>Actions</th>
           </tr>
        </thead>
        <tbody>
            @foreach ($products as $key =>$product)
            <tr>
                <td><input value="{{$product->id}}" type="checkbox" name="product" id="product-{{$product->id}}"></td>
                <td>{{$key}}</td>
                <td>{{$product->name}}</td>
                <td><img src=@if(!$product->image){{ url('default.jpg') }}   @else {{ filter_var($product->image, FILTER_VALIDATE_URL)?$product->image: url('upload',$product->image) }} @endif  alt="" width="100px" height="100px" /></td>
                <td>{{$product->stock}}</td>
                <td>{{$product->description}}</td>
                <td>${{$product->price}}</td>
                <td>{{$product->status ==1?"show":"hide"}}</td>
                <td>
                        <a href="{{route('product.edit',$product->id)}}"><span title="Edit" type="button" class="btn btn-flat btn-primary"><i class="fa fa-edit"></i></span></a>&nbsp;

                        <button  data-id="{{$product->id}}" title="Delete" class="btn btn-flat btn-danger deleteProduct"><i class="fa fa-trash"></i></button></td>
                </td>
            </tr>

            @endforeach

        </tbody>
        </table>
    </div>
    <div style="display:flex;align-self: center;justify-content: center">
        {{$products->links()}}
    </div>
    </section>
    <!-- /.box-body -->
    </div>

</div>


@endsection
