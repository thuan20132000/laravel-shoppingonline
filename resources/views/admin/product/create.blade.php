


@extends('admin.layouts.master')

@section('content')

<div class="content-wrapper">

    <section class="content">
        <div class="row">
          <div class="col-md-6">
            @if(session()->has('success'))
            <div class="alert alert-success" style="width:200ox;margin:auto">
                {{ session()->get('success') }}
            </div>
            @endif
            <div class="card card-primary mt-3">
              <div class="card-header">
                <h3 class="card-title">General</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">

                  <form action="{{route('product.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label>Category</label>
                        <select name="category" class="form-control @error('category') is-invalid @enderror " style="width: 100%;" tabindex="-1" aria-hidden="true">
                            @foreach ($categories as $category)
                                <option value="{{$category->id}}" name="" >{{$category->name}}</option>
                            @endforeach

                        </select>
                        @error('category')
                        <span class="error invalid-feedback">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label>Vendor</label>
                        <select name="vendor" class="form-control @error('vendor') is-invalid @enderror " style="width: 100%;" tabindex="-1" aria-hidden="true">
                            @foreach ($vendors as $vendor)
                                <option value="{{$vendor->id}}" name="" >{{$vendor->name}}</option>
                            @endforeach

                        </select>
                        @error('vendor')
                        <span class="error invalid-feedback">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="inputName">product Name</label>
                        <input name="name" type="text" id="inputName" class="form-control @error('name') is-invalid @enderror ">
                        @error('name')
                        <span class="error invalid-feedback">{{$message}}</span>
                        @enderror
                    </div>

                      <div class="form-group">
                        <label for="inputDescription">product Description</label>
                        <textarea name="description" id="inputDescription" class="form-control" rows="4"></textarea>
                      </div>

                      <div class="row">
                        <div class="form-group col-6">
                            <label for="inputPrice">product Price</label>
                            <input name="price" type="text" id="inputPrice" class="form-control @error('price') is-invalid @enderror ">
                            @error('price')
                            <span class="error invalid-feedback">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="form-group col-6">
                            <label for="inputStock">product Quantity</label>
                            <input name="stock" type="text" id="inputStock" class="form-control @error('stock') is-invalid @enderror ">
                            @error('stock')
                            <span class="error invalid-feedback">{{$message}}</span>
                            @enderror
                        </div>
                      </div>

                      <div class="form-group" id="uploadForm">
                          <input type="file" name="image" id="file" />
                      </div>
                      <div class="form-group">
                          <label for="productStatus">Show</label>
                          <input type="checkbox" name="status" id="productStatus" checked >
                      </div>
                      <div class="form-group">
                          <input type="submit" value="Create new Porject" class="btn btn-success float-right">
                      </div>
                  </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          {{-- col-6 --}}
      </section>

</div>
@endsection
