<!DOCTYPE html>
<html lang="en">
<head>

    @include('admin.layouts.head')
</head>


<body class="hold-transition sidebar-mini layout-fixed">
        <div class="wrapper">

            @include('admin.layouts.navbar')
            @include('admin.layouts.sidebar')


            @yield('content')


        </div>


        <!-- /.content-wrapper -->
    @include('admin.layouts.footer')
</body>
</html>
