


@extends('admin.layouts.master')

@section('content')

<div class="content-wrapper">
    @if(session()->has('success'))
        <div class="alert alert-success" style="width:200ox;margin:auto">
            {{ session()->get('success') }}
        </div>
    @endif
    <section class="content">
        <div class="row">
          <div class="col-md-6">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">General</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                  <form action="{{route('category.update',$category->id)}}" method="POST" enctype="multipart/form-data">
                    {{method_field('PUT')}}
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="inputName">Category Name</label>
                        <input value="{{$category->name}}" name="name" type="text" id="inputName" class="form-control @error('name') is-invalid @enderror ">
                        @error('name')
                        <span class="error invalid-feedback">{{$message}}</span>
                        @enderror
                    </div>

                      <div class="form-group">
                        <label for="inputDescription">Category Description</label>
                        <textarea name="description" id="inputDescription" class="form-control" rows="4">
                            {{$category->description}}
                        </textarea>
                      </div>
                      <div class="form-group" id="uploadForm">
                          <input type="file" name="image" id="file" />
                      </div>
                      <img name="image_prev" src="{{url('upload',$category->image)}}" alt="" sizes="" srcset="">
                      <div class="form-group">
                          <label for="categoryStatus">Show</label>
                          <input type="checkbox" name="status" id="categoryStatus" checked >
                      </div>
                      <div class="form-group">
                          <input type="submit" value="Update" class="btn btn-success float-right">
                      </div>
                  </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          {{-- col-6 --}}
      </section>

</div>
@endsection
