


@extends('admin.layouts.master')

@section('content')

<div class="content-wrapper">

    <section class="content">
        <div class="row">
          <div class="col-md-6">
            @if(session()->has('success'))
            <div class="alert alert-success" style="width:200ox;margin:auto">
                {{ session()->get('success') }}
            </div>
            @endif
            <div class="card card-primary mt-3">
              <div class="card-header">
                <h3 class="card-title">General</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">

                  <form action="{{route('promotion.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <label for="inputName">promotion Name</label>
                        <input value="" name="name" type="text" id="inputName" class="form-control @error('name') is-invalid @enderror ">
                        @error('name')
                        <span class="error invalid-feedback">{{$message}}</span>
                        @enderror
                    </div>

                      <div class="form-group">
                        <label for="inputDescription">promotion Description</label>
                        <textarea name="description" id="inputDescription" class="form-control" rows="4">
                        </textarea>
                      </div>

                    <div class="row">
                        <div class="form-group col-6">
                            <label for="inputStartDate">Date Start:</label>
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                              </div>
                              <input value="" name="start_date" id="inputStartDate" type="date" class="form-control @error('start_date') is-invalid @enderror ">
                                @error('start_date')
                                    <span class="error invalid-feedback">{{$message}}</span>
                                @enderror
                            </div>
                            <!-- /.input group -->
                        </div>
                        <div class="form-group col-6">
                            <label for="inputEndDate">Date End:</label>
                            <div class="input-group">
                              <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                              </div>
                              <input value="" name="end_date" id="inputEndDate" type="date" class="form-control @error('end_date') is-invalid @enderror ">
                                @error('end_date')
                                    <span class="error invalid-feedback">{{$message}}</span>
                                @enderror
                            </div>
                            <!-- /.input group -->
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-6">
                            <label for="inputPromotionCode">promotion Code</label>
                            <input value="" name="promotionCode" type="text" id="inputPromotionCode" class="form-control @error('promotionCode') is-invalid @enderror ">
                            @error('promotionCode')
                            <span class="error invalid-feedback">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="form-group col-6">
                            <label for="inputPromotionPrice">promotion Price</label>
                            <input value="" name="promotionPrice" type="text" id="inputPromotionPrice" class="form-control @error('name') is-invalid @enderror ">
                            @error('promotionPrice')
                            <span class="error invalid-feedback">{{$message}}</span>
                            @enderror
                        </div>
                    </div>


                      <div class="form-group" id="uploadForm">
                          <input type="file" name="image" id="file" />
                      </div>
                      <img name="image_prev" src="" alt="" >
                      <div class="form-group">
                          <label for="promotionStatus">Show</label>
                          <input type="checkbox" name="status" id="promotionStatus" checked >
                      </div>
                      <div class="form-group">
                          <input type="submit" value="Update" class="btn btn-success float-right">
                      </div>
                  </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          {{-- col-6 --}}
      </section>

</div>
@endsection
