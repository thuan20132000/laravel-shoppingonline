


@extends('admin.layouts.master')

@section('content')

<div class="content-wrapper">

    <section class="content">
        <div class="row">
          <div class="col-md-6">
            @if(session()->has('success'))
            <div class="alert alert-success" style="width:200ox;margin:auto">
                {{ session()->get('success') }}
            </div>
            @endif
            <div class="card card-primary mt-3">
              <div class="card-header">
                <h3 class="card-title">General</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">

                  <form action="{{route('vendor.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="inputName">vendor Name</label>
                        <input name="name" type="text" id="inputName" class="form-control @error('name') is-invalid @enderror ">
                        @error('name')
                        <span class="error invalid-feedback">{{$message}}</span>
                        @enderror
                    </div>

                      <div class="row">
                        <div class="form-group col-6">
                            <label for="inputPhone">vendor Phone</label>
                            <input name="phone" type="text" id="inputPhone" class="form-control @error('phone') is-invalid @enderror ">
                            @error('phone')
                            <span class="error invalid-feedback">{{$message}}</span>
                            @enderror
                        </div>
                        <div class="form-group col-6">
                            <label for="input">vendor Address</label>
                            <input name="address" type="text" id="inputAddress" class="form-control @error('address') is-invalid @enderror ">
                            @error('address')
                            <span class="error invalid-feedback">{{$message}}</span>
                            @enderror
                        </div>
                      </div>

                      <div class="form-group" id="uploadForm">
                          <input type="file" name="image" id="file" />
                      </div>

                      <div class="form-group">
                          <input type="submit" value="Create new Porject" class="btn btn-success float-right">
                      </div>
                  </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          {{-- col-6 --}}
      </section>

</div>
@endsection
