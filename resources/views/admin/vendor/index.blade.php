


@extends('admin.layouts.master')

@section('content')
<div class="content-wrapper">



    <section class="p-3">
            <div class="d-flex justify-content-end" style="width:100%">
                <div style="width:120px;margin-right:20px">
                    <select name="" id=""  class="btn btn-success"  style="width:100%;height:100%">
                        <option value="1">All data</option>
                        <option value="1">1</option>
                        <option value="1">1</option>

                    </select>
                </div>
                <div class="d-flex">
                        <input type="text" value="" placeholder="Search Email or Id" style="width:200px">
                        <button>Search</button>
                </div>
            </div>
    </section>
    <section>
        <div class="d-flex justify-content-between">
            <div style="margin-left:10px;display:flex;flex-direction: row;align-items: center">
                <div><input type="checkbox" name="checkAll" id="checkvendor-all"></div>
                <div style="margin-left:20px"><Button class="btn btn-danger" id="deletevendor-checked">Delete</Button></div>
                <div style="margin-left:20px"><button class="btn btn-success">Refresh</button></div>
                <div style="margin-left:20px"><button>ID Desc</button></div>
                <div style="margin-left:20px"><button>Sort</button></div>
            </div>
            <div style="margin-right:20px">
                <a href="{{route('vendor.create')}}" class="btn btn-success">Add new</a>
            </div>
        </div>
    </section>

    <!-- /.box-header -->
    <section id="pjax-container" class="table-list">
    <div class="box-body table-responsive no-padding" >
        <table class="table table-hover">
        <thead>
           <tr>
               <th></th>
               <th>STT</th>
               <th>Name</th>
               <th>Image</th>
               <th>Address</th>
               <th>Phone</th>
               <th>Actions</th>
           </tr>
        </thead>
        <tbody>
            @foreach ($vendors as $key =>$vendor)
            <tr>
                <td><input value="{{$vendor->id}}" type="checkbox" name="vendor" id="vendor-{{$vendor->id}}"></td>
                <td>{{$key}}</td>
                <td>{{$vendor->name}}</td>
                <td><img src=@if(!$vendor->image){{ url('default.jpg') }}   @else {{ filter_var($vendor->image, FILTER_VALIDATE_URL)?$vendor->image: url('upload',$vendor->image) }} @endif  alt="" width="100px" height="100px" /></td>
                <td>{{$vendor->address}}</td>
                <td>{{$vendor->phone}}</td>
                <td>
                        <a href="{{route('vendor.edit',$vendor->id)}}"><span title="Edit" type="button" class="btn btn-flat btn-primary"><i class="fa fa-edit"></i></span></a>&nbsp;

                        <button  data-id="{{$vendor->id}}" title="Delete" class="btn btn-flat btn-danger deletevendor"><i class="fa fa-trash"></i></button></td>
                </td>
            </tr>

            @endforeach

        </tbody>
        </table>
    </div>
    <div style="display:flex;align-self: center;justify-content: center">
        {{$vendors->links()}}
    </div>
    </section>
    <!-- /.box-body -->
    </div>

</div>


@endsection
