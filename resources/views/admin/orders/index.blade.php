


@extends('admin.layouts.master')

@section('content')
<div class="content-wrapper">



    <section class="p-3">
            <div class="d-flex justify-content-end" style="width:100%">
                <div style="width:120px;margin-right:20px">
                    <select name="" id=""  class="btn btn-success"  style="width:100%;height:100%">
                        <option value="1">All data</option>
                        <option value="1">1</option>
                        <option value="1">1</option>

                    </select>
                </div>
                <div class="d-flex">
                        <input type="text" value="" placeholder="Search Email or Id" style="width:200px">
                        <button>Search</button>
                </div>
            </div>
    </section>
    <section>
        <div class="d-flex justify-content-between">
            <div style="margin-left:10px;display:flex;flex-direction: row;align-items: center">
                <div><input type="checkbox" name="checkAll" id="checkorder-all"></div>
                <div style="margin-left:20px"><Button class="btn btn-danger" id="deleteorder-checked">Delete</Button></div>
                <div style="margin-left:20px"><button class="btn btn-success">Refresh</button></div>
                <div style="margin-left:20px"><button>ID Desc</button></div>
                <div style="margin-left:20px"><button>Sort</button></div>
            </div>
            <div style="margin-right:20px">
                <a href="{{route('order.create')}}" class="btn btn-success">Add new</a>
            </div>
        </div>
    </section>

    <!-- /.box-header -->
    <section id="pjax-container" class="table-list">
    <div class="box-body table-responsive no-padding" >
        <table class="table table-hover">
        <thead>
           <tr>
               <th></th>
               <th>STT</th>
               <th>User</th>
               <th>Subprice</th>
               <th>Shipping</th>
               <th>Discount</th>
               <th>Total</th>
               <th>PM Method</th>
               <th>Status</th>
               <th>Actions</th>
           </tr>
        </thead>
        <tbody>
            @foreach ($orders as $key =>$order)
            <tr>
                <td><input value="{{$order->id}}" type="checkbox" name="order" id="order-{{$order->id}}"></td>
                <td>{{$key}}</td>
                <td>{{$order->user->email}}</td>
                <td>${{$order->subtotal+$order->discount}}</td>
                <td>{{$order->shipping}}</td>
                <td>{{$order->discount}}</td>
                <td>${{($order->subtotal + $order->shipping)}}</td>
                <td>{{$order->payment_method}}</td>
                <td>{{$order->status ==1?"show":"hide"}}</td>
                <td>
                        <a href="{{route('order.show',$order->id)}}"><span title="Edit" type="button" class="btn btn-flat btn-primary"><i class="far fa-eye"></i></span></a>&nbsp;
                        <button  data-id="{{$order->id}}" title="Delete" class="btn btn-flat btn-danger deleteorder"><i class="fa fa-trash"></i></button></td>
                </td>
            </tr>

            @endforeach

        </tbody>
        </table>
    </div>
    <div style="display:flex;align-self: center;justify-content: center">
        {{$orders->links()}}
    </div>
    </section>
    <!-- /.box-body -->
    </div>

</div>


@endsection
