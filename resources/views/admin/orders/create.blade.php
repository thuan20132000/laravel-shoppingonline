


@extends('admin.layouts.master')

@section('content')
<script src="{{asset('admin/plugins/jquery/jquery.min.js')}}"></script>

<div class="content-wrapper">
    <section class="content">
        <div class="row">
          <div class="col-md-6">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Customer Information</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="icheck-primary d-inline" style="float:right">
                    <input type="checkbox" id="inputNewCustomer" >
                    <label for="inputNewCustomer">
                      New Customer
                    </label>
                </div>
                <div class="form-group dropdown-customer">
                  <label for="inputQueryCustomer">Enter Customer</label>
                  <input autocomplete="off" type="text" id="inputQueryCustomer" class="form-control dropdown-btn-customer customer-name">
                  <div class="dropdown-content-customer" id="show-query-customer">
                    {{--  --}}

                  </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail">Email</label>
                    <input type="text" id="inputEmail" class="form-control customer-email">
                </div>
                <div class="form-group">
                    <label for="inputPhone">Phone</label>
                    <input type="text" id="inputPhone" class="form-control customer-phone">
                </div>

                <fieldset>
                <legend>Order Address</legend>
                    <div class="form-group">
                        <label for="inputProvince">Province</label>
                        <select class="form-control select2 select2-hidden-accessible" id="inputProvince" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                            <option selected="selected" data-select2-id="3">Selecting Province</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="inputDistrict">Disctrict</label>
                        <select class="form-control select2 select2-hidden-accessible" id="inputDistrict" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                            <option selected="selected" data-select2-id="3">Selecting District</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="inputSubDistrict">Sub-Disctrict</label>
                        <select class="form-control select2 select2-hidden-accessible" id="inputSubDistrict" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                            <option selected="selected" data-select2-id="3">Selecting Sub District</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="inputStreet">Street</label>
                        <textarea class="form-control" name="" id="inputStreet" cols="30" rows="4"></textarea>
                    </div>
                </fieldset>
                <div class="form-group">
                    <label for="inputPromotionCode">Promotion Code</label>
                    <input type="text" id="inputPromotionCode" class="form-control">
                    <p class="promotion-code-status"></p>
                </div>
                <div class="form-group">
                    <label for="inputShippingMethod">Shipping Method</label>
                    <select class="form-control select2 select2-hidden-accessible" id="inputShippingMethod" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                        <option selected="selected" value="0">Standard</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="inputPaymentMethod">Payment Method</label>
                    <select class="form-control select2 select2-hidden-accessible" id="inputPaymentMethod" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true">
                        <option selected="selected" value="0">Cash on Delivery</option>
                        <option  value="1">Paypal</option>
                    </select>
                </div>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <div class="col-12">
                <a href="#" class="btn btn-secondary">Cancel</a>
                <input type="submit" disabled  id="order-product" value="Create New Order" class="btn btn-success float-right">
              </div>
          </div>

          <div class="col-md-6">
            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Product Information</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group dropdown">
                  <label for="inputQueryProduct">Enter Product</label>
                  <input type="text" id="inputQueryProduct" class="form-control dropdown-btn" placeholder="Enter Product Name">
                  <div class="dropdown-content" id="show-query-product">
                    {{-- Products  --}}
                  </div>
                </div>
                <div class="btn btn-success totalPrice-wrapper">
                    <span>Total Price: </span> <span id="show-totalPrice"></span>
                </div>
                <div class="list-group" id="show-product-added">

                </div>


                {{-- Product show here --}}
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>

        {{--  --}}
      </section>

</div>

<script>



    async function fetch_province(){
    const data = await fetch('https://raw.githubusercontent.com/madnh/hanhchinhvn/master/dist/tinh_tp.json');
        var res = await data.json();
        await print_provinces(res);
    }
    fetch_province();
    function print_provinces(data){
            var provinceOption = "";
            Object.values(data).forEach(e => {
                provinceOption  += '<option value="'+e.code+'">'+e.name+'</option>';
            });

            $('#inputProvince').append(provinceOption);
    }
    $('#inputProvince').on("change",function(){
        var provinceCode = $(this).val();
        $('#inputDistrict').html("").append('<option>Selecting District</option>');
        $('#inputSubDistrict').html("").append('<option>Selecting Sub District</option>');
        fetch_district(provinceCode);
    });

    // ===== //
    async function fetch_district(provinceCode){
    const data = await fetch(`https://raw.githubusercontent.com/madnh/hanhchinhvn/master/dist/quan-huyen/${provinceCode}.json`);
        var res = await data.json();
        print_district(res);
    }
    function print_district(data){
            var districtOption = "";
            Object.values(data).forEach(e => {
                districtOption  += '<option value="'+e.code+'">'+e.name_with_type+'</option>';
            });

            $('#inputDistrict').append(districtOption);
    }
    $('#inputDistrict').on("change",function(){
        var districtCode = $(this).val();
        fetch_subDistrict(districtCode);
    });
    // ===== //
    async function fetch_subDistrict(districtCode){
    const data = await fetch(`https://raw.githubusercontent.com/madnh/hanhchinhvn/master/dist/xa-phuong/${districtCode}.json`);
        var res = await data.json();
        print_subDistrict(res);
    }
    function print_subDistrict(data){
            $('#inputSubDistrict').html("");
            var subDistrictOption = "";
            Object.values(data).forEach(e => {
                subDistrictOption  += '<option value="'+e.code+'">'+e.name_with_type+'</option>';
            });

            $('#inputSubDistrict').html(subDistrictOption);
    }




$('document').ready(function(){





    //
    function debounce(fn, delay, immediate) {
        let timeout;

        return function executedFn() {
            let context = this; // "this" context của executedFn
            let args = arguments; // "arguments" của fn

            let later = function() {
                timeout = null;
                if (!immediate) fn.apply(context, args);
            };

            let callNow = immediate && !timeout;

            clearTimeout(timeout);

            timeout = setTimeout(later, delay);

            if (callNow) fn.apply(context, args);
        }
    }



    //create product object
    function ProductObject(id,name,price,image,quantity){
        this.id = id;
        this.name =name;
        this.price = price;
        this.image = image;
        this.quantity = quantity;
    }
    var productAddedArray = [];


    const _handleSearchProduct = event => {
        // do something with event

        var query = $('#inputQueryProduct').val();
        fetch_product_data(query);
    };

    const debouncedKeyUp = debounce(_handleSearchProduct, 500);
    $('#inputQueryProduct').on('keyup', debouncedKeyUp);


    function fetch_product_data(query = "") {
        $.ajax({
            method: 'GET',
            url: '../searchProducts',
            data: {
                query: query
            },
            dataType: 'JSON',
            success: function(data) {
                if (data.data != "") {

                    var tempProduct = [];
                    data.data.forEach(e => {
                    var newProduct  = new ProductObject(e.id,e.name,e.price,e.image,1);
                        tempProduct.push(newProduct);
                    });
                    displaySearchProduct(tempProduct);


                } else {
                    $('#show-query-product').empty();
                }
            }
        });
    }

    function displaySearchProduct(tempProduct){
        var productContain = "";
        tempProduct.forEach(e=>{
            productContain +="<div class='productWrapper'>"
                +"<div><img width='100px' src='{{ url('default.jpg') }}' /></div>"
                +"<div class='product-name'>"+e.name+"</div>"
                +"<div class='btn btn-success btn-add-product' data-id="+e.id+" data-price="+e.price+" data-image="+e.image+" data-name="+e.name+" data-quantity="+e.quantity+"  >Add</div>"
                +"</div>";


        });
        $('#show-query-product').html(productContain);

        getAddedProduct();

    }

    function getAddedProduct(){
        $('.btn-add-product').on('click',function(){
            let getName = $(this).attr("data-name");
            let getId = $(this).attr("data-id");
            let getImage = $(this).attr("data-image");
            let getPrice = $(this).attr("data-price");
            let getQuantity = $(this).attr("data-quantity");

            if(checkProductDuplicate(getId)){
               return ;
            }

                let getProduct = new ProductObject(getId,getName,getPrice,getImage,getQuantity);
                productAddedArray.push(getProduct);
                displayAddedProduct();
                _handleProductTotalPrice();
                checkProductDuplicate();
                checkEmptyOrder();

        });

    }

    function checkProductDuplicate(newProductId){

        var isExist =  productAddedArray.find(product => product.id === newProductId);
        return isExist;

    }


    function displayAddedProduct(){

        var productAdded = '';
        productAddedArray.forEach(e=>{
            productAdded += '<div class="card card-added-product" data-id='+e.id+' >'
                +'<h5 class="card-header">'+e.name+'</h5>'
                +'<div class="card-body" style="display:flex;flex-direction:row">'
                +"<div class='card-text'><img width='100px' src='{{ url('default.jpg') }}' /></div>"
                +"<div><label>Enter quantity:</label><input class='product-quantity' type='text' value='"+e.quantity+"' /></div>"
                +"<div><button class='btn btn-danger btn-remove-product'>Remove</button></div>"
                +"</div><p style='text-align:center'>$"+Number(e.price)*Number(e.quantity)+"<p/> </div>";
        })
        $('#show-product-added').html(productAdded);

        removeAddedProduct();
        _handleProductQuantity();

        if(productAddedArray.length <=0){
            $('#show-query-product').append('<p>No Products found!!</p>');
        }
    }

    function removeAddedProduct(){
        $('.btn-remove-product').on('click',function(){
            var currentProduct =  $(this).parent().parent().parent().attr('data-id');

            productAddedArray.forEach((e,index)=>{
                if(currentProduct === e.id){
                    productAddedArray.splice(index,1);
                    displayAddedProduct();
                }
            });
            checkEmptyOrder();
            _handleProductTotalPrice();

         });
    }


    function _handleProductQuantity(){
        $('.product-quantity').on('change',function(){
            var quantity =  $(this).val();
            var currentProductId = $(this).parent().parent().parent().attr('data-id');
            productAddedArray.forEach((e)=>{
                if(e.id === currentProductId){
                    e.quantity = quantity;
                    displayAddedProduct();
                }
            });

            _handleProductTotalPrice();
        })
    }

    function _handleProductTotalPrice(){

        var totalPrice = 0;
        productAddedArray.forEach((e)=>{
            totalPrice += e.price *e.quantity;
        });
        $('#show-totalPrice').html('  $'+totalPrice);

        return totalPrice;
    }

    function isEmptyOrder(){
        if(productAddedArray.length<=0){
            return true;
        }
        return false;
    }
    function checkEmptyOrder(){
        if(isEmptyOrder()){
            $('#order-product').prop("disabled",true);
        }else{
            $('#order-product').removeAttr("disabled");
        }
    }




{{-- Customer Handler --}}
    const _handleSearchCustomer = event => {
        // do something with event
        var query = $('#inputQueryCustomer').val();
        fetch_customer_data(query);
    };

    const queryCustomerKeyUp = debounce(_handleSearchCustomer, 500);
    $('#inputQueryCustomer').on('keyup', queryCustomerKeyUp);

    function CustomerObject(id,name,email,image,phone){
        this.id = id;
        this.name = name;
        this.image = image;
        this.email = email;
        this.phone = phone;
    }
    function fetch_customer_data(query = "") {
        $.ajax({
            method: 'GET',
            url: '../searchCustomers',
            data: {
                query: query
            },
            dataType: 'JSON',
            success: function(data) {
                console.log(data);
                if (data.data != "") {

                    var tempCustomer = [];
                    data.data.forEach(e => {
                    var newCustomer  = new CustomerObject(e.id,e.name,e.email,'default.jpg',e.phone);
                        tempCustomer.push(newCustomer);

                    });
                    displaySearchCustomer(tempCustomer);


                } else {
                    $('#show-query-product').empty();
                }
            }
        });

    }

    function displaySearchCustomer(tempCustomer){
        var customerContain = "";
        tempCustomer.forEach(e=>{
            customerContain += '<div class="customer-query-card" data-id='+e.id+'>'
               +'<div><img width="100px" src={{ url("default.jpg") }} /></div>'
                +'<div class="customer-query-text">'
                    +'<div style="font-weight:bold">'+e.name+'</div>'
                    +'<div>'+e.email+'</div>'
                +'</div>'
                +' </div>';
        });
        $('#show-query-customer').html(customerContain);

        getAddedCustomer();
    }

    function getAddedCustomer(){
        $('.customer-query-card').on('click',function(e){
            customerId =  $(this).attr('data-id');
            fetch_customer_info(customerId);
            e.preventDefault();
        })


    }

    function fetch_customer_info(id){

        $.ajax({
            method: 'GET',
            url: '../fetchCustomerInfo',
            data: {
                id: id
            },
            dataType: 'JSON',
            success: function(data) {
                if (data.data != "") {
                    print_customer_info(data);
                }
                if(data.orderAddress != null){
                    console.log(data.orderAddress);
                    print_order_address(data.orderAddress);
                }
            }
        });
    }

    function print_order_address(address){
        console.log(address.province);
        $('#inputProvince option:selected').text(address.province);
        $('#inputDistrict option:selected').text(address.district);
        $('#inputSubDistrict option:selected').text(address.subDistrict);
        $('#inputStreet').val(address.address);


    }

    var customerInfo = '';
    function print_customer_info(customer){
        var customer = customer.data[0];

        if(customer){
            $('.customer-name').val(customer.name);
            $('.customer-phone').val(customer.phone);
            $('.customer-email').val(customer.email);
            $('.customer-address').val(customer.address);
            customerInfo = new CustomerObject(customer.id,customer.name,
            customer.email,customer.image,customer.address);
            return ;
        }

    }
    $('#inputNewCustomer').on('change',function(){

        if($(this).prop("checked")){
            $('#show-query-customer').html("");
            $('.customer-name').removeAttr('id');

            return;
        }else{
            $('.customer-name').attr('id','inputQueryCustomer');
        }
    });



    var orderAddress = [];
    function AddressObject(province,district,subDistrict,street){
        this.province = province;
        this.district  = district;
        this.subDistrict = subDistrict;
        this.street = street;
    }

    function getOrderAddress(){
        var province = "";
        var district = "";
        var subDistrict = "";
        var street = "";

        province =  $('#inputProvince option:selected').text();
        district = $('#inputDistrict option:selected').text();
        subDistrict = $('#inputSubDistrict option:selected').text();
        street = $('#inputStreet').val();

        let address = new AddressObject(province,district,subDistrict,street);
        orderAddress.push(address);

        return address;
    }


    $('#order-product').on('click',function(){

        if($('#inputNewCustomer').prop("checked")){
            console.log("checked");
            customerInfo = null;
            let name =  $('.customer-name').val();
            let phone = $('.customer-phone').val();
            let email = $('.customer-email').val();
            customerInfo = new CustomerObject(null,name,email,phone);
        }
        paymentMethod = $('#inputPaymentMethod option:selected').text();


        _handleOrderProduct();
    });

    var promotionPrice = null;
    var paymentMethod = null;
    const _handleOrderProduct = async () =>{
        var totalPrice =  await _handleProductTotalPrice();
        var address = await getOrderAddress();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            method: 'POST',
            url: '../order',
            data: {
               products : productAddedArray,
               totalPrice : totalPrice,
               customer : customerInfo,
               address : address,
               discount : promotionPrice,
               payment : paymentMethod,
            },
            dataType: 'JSON',
            success: function(data) {
                console.log(data);
                if (data.data == null) {
                   toastr.success('Create Order Successfull',{timeout:3000});
                    setTimeout(function(){
                        window.location.href = '/admin/order';
                    },1500);

                }else{
                    toastr.error('Create Order Failed',{timeout:3000});

                }
            }
        });

    }

    //Promotion Code
    const _handleSearchPromotionCode = event => {
        // do something with event
        var query = $('#inputPromotionCode').val();
        checkPromotionCode(query);
    };

    const queryPromotionCode = debounce(_handleSearchPromotionCode, 500);
    $('#inputPromotionCode').on('keyup', queryPromotionCode);

    function checkPromotionCode(query){
        $.ajax({
            method: 'GET',
            url: '../searchPromotionCode',
            data: {
                query: query
            },
            dataType: 'JSON',
            success: function(data) {
                if (data.data != "") {
                    promotionPrice = data.data[0].promotionPrice;
                    $('.promotion-code-status').html("<span style='color:coral'> You just received a discount of $"+data.data[0].promotionPrice+"</span");
                }else{
                    $('.promotion-code-status').html("<span style='color:red'>Promotion Code not found!!</span>");
                }
            }
        });
    }








});
</script>

<style>
    .productWrapper{
        display: flex;
        flex-direction: row;
        flex-wrap:wrap;
        margin-top: 10px;
        justify-content: space-between;
        align-items: center

    }
    .productWrapper:hover{
        background-color: azure;
        cursor: pointer;
    }
    .product-name{
        padding: 20px;
    }
    .btn-add a{
        color:white
    }
    .dropbtn {
        background-color: #4CAF50;
        color: white;
        padding: 16px;
        font-size: 16px;
        border: none;

    }

      .dropdown {
        position: relative;
        display: inline-block;
        width: 100%;
      }

      .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
        width: 100%;
      }

      .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
      }

      .dropdown-content a:hover {background-color: #ddd;}

      .dropdown:hover .dropdown-content {display: block;}

      .dropdown:hover .dropbtn {background-color: #3e8e41;}

      {{-- customer query --}}
      .dropbtn-customer {
        background-color: #4CAF50;
        color: white;
        padding: 16px;
        font-size: 16px;
        border: none;

    }

      .dropdown-customer {
        position: relative;
        display: inline-block;
        width: 100%;
      }

      .dropdown-content-customer {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
        width: 100%;
      }

      .dropdown-content-customer a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
      }

      .dropdown-content-customer a:hover {background-color: #ddd;}

      .dropdown-customer:hover .dropdown-content-customer {display: block;}

      .dropdown-customer:hover .dropbtn-customer {background-color: #3e8e41;}
      .customer-query-card{
          display: flex;
          align-items: center;

      }
      .customer-query-text{
          padding-left: 20px;
      }
      .customer-query-card:hover{
          background-color: aliceblue;
          cursor: pointer;
      }
      #show-totalPrice{
        font-weight: bold;
        color: cornsilk;
      }
      .totalPrice-wrapper{
        display: flex;
        justify-content: center;
      }
</style>
@endsection

