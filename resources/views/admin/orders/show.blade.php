


@extends('admin.layouts.master')

@section('content')
<script src="{{asset('admin/plugins/jquery/jquery.min.js')}}"></script>

<div class="content-wrapper">
    <section class="content">
        <div class="row">
          <div class="col-md-6">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Customer Information</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="form-group dropdown-customer">
                  <label for="inputQueryCustomer">Enter Customer</label>
                  <input disabled type="text" value="{{$order->user->name}}" id="inputQueryCustomer" class="form-control dropdown-btn-customer customer-name">
                  <div class="dropdown-content-customer" id="show-query-customer">
                    {{--  --}}

                  </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail">Email</label>
                    <input disabled type="text" value="{{$order->user->email}}" id="inputEmail" class="form-control customer-email">
                </div>
                <div class="form-group">
                    <label for="inputPhone">Phone</label>
                    <input disabled type="text" value="{{$order->user->phone}}" id="inputPhone" class="form-control customer-phone">
                </div>
                <div class="form-group">
                    <label for="inputAddress">Address</label>
                    <input disabled type="text" value="{{$order->user->address}}" id="inputAddress" class="form-control customer-address">
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

          </div>

          <div class="col-md-6">
            <div class="card card-secondary">
              <div class="card-header">
                <h3 class="card-title">Product Information</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">
                <div class="btn btn-success totalPrice-wrapper">
                    <span>Total Price: </span> <span id="show-totalPrice">{{$order->subtotal}}</span>
                </div>
                <div class="list-group" id="show-product-added">
                    @foreach ($order->orderDetail as $order)
                    <div class="card card-added-product" data-id='+e.id+' >
                        <h5 class="card-header">{{$order->product->name}}</h5>
                        <div class="card-body" style="display:flex;flex-direction:row">
                            <div class='card-text'><img width='100px' src='{{ url('default.jpg') }}' /></div>
                            <div><label>Quantity:</label> <span>{{$order->quantity}}</span></div>
                        </div>
                    </div>
                    @endforeach

                </div>


                {{-- Product show here --}}
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>


        <div class="row">

        </div>




        {{--  --}}
      </section>

</div>

<script>

</script>

<style>
    .productWrapper{
        display: flex;
        flex-direction: row;
        flex-wrap:wrap;
        margin-top: 10px;
        justify-content: space-between;
        align-items: center

    }
    .productWrapper:hover{
        background-color: azure;
        cursor: pointer;
    }
    .product-name{
        padding: 20px;
    }
    .btn-add a{
        color:white
    }
    .dropbtn {
        background-color: #4CAF50;
        color: white;
        padding: 16px;
        font-size: 16px;
        border: none;

    }

      .dropdown {
        position: relative;
        display: inline-block;
        width: 100%;
      }

      .dropdown-content {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
        width: 100%;
      }

      .dropdown-content a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
      }

      .dropdown-content a:hover {background-color: #ddd;}

      .dropdown:hover .dropdown-content {display: block;}

      .dropdown:hover .dropbtn {background-color: #3e8e41;}

      {{-- customer query --}}
      .dropbtn-customer {
        background-color: #4CAF50;
        color: white;
        padding: 16px;
        font-size: 16px;
        border: none;

    }

      .dropdown-customer {
        position: relative;
        display: inline-block;
        width: 100%;
      }

      .dropdown-content-customer {
        display: none;
        position: absolute;
        background-color: #f1f1f1;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
        width: 100%;
      }

      .dropdown-content-customer a {
        color: black;
        padding: 12px 16px;
        text-decoration: none;
        display: block;
      }

      .dropdown-content-customer a:hover {background-color: #ddd;}

      .dropdown-customer:hover .dropdown-content-customer {display: block;}

      .dropdown-customer:hover .dropbtn-customer {background-color: #3e8e41;}
      .customer-query-card{
          display: flex;
          align-items: center;

      }
      .customer-query-text{
          padding-left: 20px;
      }
      .customer-query-card:hover{
          background-color: aliceblue;
          cursor: pointer;
      }
      #show-totalPrice{
        font-weight: bold;
        color: cornsilk;
      }
      .totalPrice-wrapper{
        display: flex;
        justify-content: center;
      }
</style>
@endsection

