


@extends('admin.layouts.master')

@section('content')
<div class="content-wrapper">



    <section class="p-3">
            <div class="d-flex justify-content-end" style="width:100%">
                <div style="width:120px;margin-right:20px">
                    <select name="" id=""  class="btn btn-success"  style="width:100%;height:100%">
                        <option value="1">All data</option>
                        <option value="1">1</option>
                        <option value="1">1</option>

                    </select>
                </div>
                <div class="d-flex">
                        <input type="text" value="" placeholder="Search Email or Id" style="width:200px">
                        <button>Search</button>
                </div>
            </div>
    </section>
    <section>
        <div class="d-flex justify-content-between">
            <div style="margin-left:10px;display:flex;flex-direction: row;align-items: center">
                <div><input type="checkbox" name="checkAll" id="checkuser-all"></div>
                <div style="margin-left:20px"><Button class="btn btn-danger" id="deleteuser-checked">Delete</Button></div>
                <div style="margin-left:20px"><button class="btn btn-success">Refresh</button></div>
                <div style="margin-left:20px"><button>ID Desc</button></div>
                <div style="margin-left:20px"><button>Sort</button></div>
            </div>
            <div style="margin-right:20px">
                <a href="" class="btn btn-success">Add new</a>
            </div>
        </div>
    </section>

    <!-- /.box-header -->
    <section id="pjax-container" class="table-list">
    <div class="box-body table-responsive no-padding" >
        <table class="table table-hover">
        <thead>
           <tr>
               <th></th>
               <th>STT</th>
               <th>Name</th>
               <th>Image</th>
               <th>Email</th>
               <th>Role</th>
               <th>Actions</th>
           </tr>
        </thead>
        <tbody>
            @foreach ($users as $key =>$user)
            <tr>
                <td><input value="{{$user->id}}" type="checkbox" name="user" id="user-{{$user->id}}"></td>
                <td>{{$key}}</td>
                <td>{{$user->name}}</td>
                <td><img src=@if(!$user->image){{ url('default.jpg') }}   @else {{ filter_var($user->image, FILTER_VALIDATE_URL)?$user->image: url('upload',$user->image) }} @endif  alt="" width="100px" height="100px" /></td>
                <td>{{$user->email}}</td>
                <td>{{  implode(',',$user->roles()->get()->pluck('name')->toArray()) }}</td>
                <td>
                        @can('edit-users')
                        <a href="{{route('admin.users.edit',$user->id)}}"><span title="Edit" type="button" class="btn btn-flat btn-primary"><i class="fa fa-edit"></i></span></a>&nbsp;
                        @endcan

                        @can('delete-users')
                        <a href="#" onclick="document.getElementById('delete-user').submit()">
                            <span title="Edit" type="button" class="btn btn-flat btn-danger"><i class="fa fa-trash"></i></span>
                        </a>&nbsp;
                        <form action="{{route('admin.users.destroy',['user'=>$user->id])}}" method="POST" id="delete-user">
                            @csrf
                            @method('DELETE')
                        </form>
                        @endcan

                </td>
            </tr>

            @endforeach

        </tbody>
        </table>
    </div>
    <div style="display:flex;align-self: center;justify-content: center">
        {{$users->links()}}
    </div>
    </section>
    <!-- /.box-body -->
        <div class="get-session">
            @if(session()->has('denied'))
                <div id="denied">
                </div>
             @endif
        </div>
    </div>

</div>


@endsection
