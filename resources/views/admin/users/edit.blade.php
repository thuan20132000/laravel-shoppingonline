


@extends('admin.layouts.master')

@section('content')

<div class="content-wrapper">

    <section class="content">
        <div class="row">
          <div class="col-md-6">
            @if(session()->has('success'))
            <div class="alert alert-success" style="width:200ox;margin:auto">
                {{ session()->get('success') }}
            </div>
            @endif
            <div class="card card-primary mt-3">
              <div class="card-header">
                <h3 class="card-title">General</h3>
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              <div class="card-body">

                  <form action="{{route('admin.users.update',$user->id)}}" method="POST" enctype="multipart/form-data">
                    {{method_field('PUT')}}
                    @csrf

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{$user->email }}">

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                        <div class="col-md-6">
                            <input id="name" type="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $user->name }}" >

                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="roles[]" class="col-md-4 col-form-label text-md-right">{{ __('Roles') }}</label>

                        <div class="col-md-6">
                            @foreach ($roles as $key => $role)
                            <div>
                                <input class="" @if($user->roles->pluck('id')->contains($role->id)) checked @endif type="checkbox" value="{{$role->id}}" name="roles[]" id="userRole-{{$role->id}}" >
                                <label for="userRole-{{$role->id}}">{{$role->name}}</label>
                            </div>
                            @endforeach
                        </div>

                    </div>


                      <div class="form-group">
                          <input type="submit" value="Update" class="btn btn-success float-right">
                      </div>
                  </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          {{-- col-6 --}}
      </section>

</div>
@endsection
